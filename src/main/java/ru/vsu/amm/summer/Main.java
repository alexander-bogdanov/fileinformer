package ru.vsu.amm.summer;

import java.awt.FlowLayout;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;


import javax.swing.*;

/**
 * Create by vmakeeva on 18/07/14.
 */
public class Main extends JFrame {

	public Main() {
        // Заголовок окна
        super("Файл-Информатор");

        setLayout(new FlowLayout());

        // Инициализируем компонент JFilePicker
        JFilePicker filePicker = new JFilePicker("Выбрать файл", "Обзор");

		// Обращаемся к JFileChooser
        JFileChooser fileChooser = filePicker.getFileChooser();
        fileChooser.setCurrentDirectory(new File("~/Downloads/"));       // Указываем стартовую директорию

        // Добавляем наш filePicker на графическую форму
        add(filePicker);


        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(600, 500);                                      // Размер окна
        setLocationRelativeTo(null);                            // Центрируем панель

	}

	/*
	 * main-метод, инициализируем наш класс FileInformer.
	 */
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new Main().setVisible(true);
            }
        });
    }

}
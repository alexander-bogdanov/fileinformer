package ru.vsu.amm.summer;


import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.*;
import java.util.List;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;

/**
 * Create by vmakeeva on 18/07/14.
 */
// Наш класс наследуется от Панели
public class JFilePicker extends JPanel {

	// Инициализация модели
	MyModel model;

	private JPanel top = new JPanel(new FlowLayout(FlowLayout.CENTER, 5, 5));
	private JLabel label;				// текст рядом с путем файла
	private JTextField textField;        // путь файла
	private JButton button;                // кнопка Обзор
	private JFileChooser fileChooser;
	private Map<String, List<String>> types = new HashMap<>();

	/**
	 * Конструктор. Аргументы - текст лейбла и текст на кнопке.
	 */
	public JFilePicker(String textFieldLabel, String buttonLabel) {

		// Сначала инициализируем JFileChooser
		fileChooser = new JFileChooser();
		fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		fileChooser.setAcceptAllFileFilterUsed(false);

		// Устанавливаем шаблон внешнего вида окна
		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));

		// Задаем графические элементы
		label 		= new JLabel(textFieldLabel);
		textField 	= new JTextField(30);
		button 		= new JButton(buttonLabel);

		// Инициализируем сначала пустую таблицу
		String[][] rowAndColumn = {{"", ""},{"", ""}};
		String[] header 		= new String[] {"Тип файла", "Количество"};

		// На основе модели рисуем таблицу
		model 					= new MyModel(rowAndColumn, header);
		JTable table 			= new JTable(model);

		// Устанавливаем обработчик щелчка по кнопке
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				buttonActionPerformed(evt);
			}
		});

		// Размещаем элементы на графической форме
		top.add(label);
		top.add(textField);
		top.add(button);
		add(top);
		add(new JScrollPane(table));
	}

	// Получение типа файла
	public static String getFileType(File file) {
		// Разбиваем строку вида "fileName.docx" на массив из элементов fileName и docx (split по точке)
		String[] row = file.getName().split("\\.");
		return row[row.length - 1];
	}

	// Получение имени файла без типа
	public static String getFileName(File file) {
		// Разбиваем строку вида "fileName.docx" на массив из элементов fileName и docx (split по точке)
		String[] row = file.getName().split("\\.");
		String result = "";

		for (int i = 0; i < row.length - 1; i++){
			result += row[i];
		}

		return result;
	}

	// Метод обработчика щелчка по кнопке
	private void buttonActionPerformed(ActionEvent evt) {
		if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
			textField.setText(fileChooser.getSelectedFile().getAbsolutePath());

			File folder = fileChooser.getSelectedFile();	// Выбранная папка;
			File[] listOfFiles = folder.listFiles();		// Список файлов в выбранной папке

			// Очищаем табличку
			model.flush();

			// Если файлы в выбранной папке существуют
			if (listOfFiles != null) {
				// Перебираем их все
				for (File file : listOfFiles) {
					// Если текущий файл является вложенной папкой - отбрасываем
					if (!file.isDirectory()) {
						String currentType = getFileType(file);	// тип файла
						String currentName = getFileName(file);	// имя файла

						// Получаем список файлов текущего типа
						List<String> currentList = types.get(currentType);
						if (currentList == null){
							currentList = new ArrayList<String>();
						}
						// Добавляем в него имя текущего файла
						currentList.add(currentName);
						// Пихаем этот список обратно в Map
						types.put(currentType, currentList);
					}
				}
			}

			// Добавляем инфу о всех типах файлов в табличку
			for (Map.Entry<String, List<String>> entry : types.entrySet()){
				String[] newRow = {entry.getKey(), entry.getValue().size() + ""};
				model.addRow(newRow);
			}

			// Если в папке нет файлов
			if (types.isEmpty()){
				String[] newRow = {"В выбранной папке нет файлов", ""};
				model.addRow(newRow);
			}

			// Перерисовываем табличку
			repaint();
		}
	}

	// Метод для получения пути к выбранному файлу
	public String getSelectedFilePath() {
		return textField.getText();
	}

	// Получаем JFileChooser непосредственно из нашего компонента JFilePicker
	public JFileChooser getFileChooser() {
		return this.fileChooser;
	}

	// Класс модели наследуется от абстрактной модели
	class MyModel extends AbstractTableModel {
		// Каждая строка таблицы хранится в arraylist
		ArrayList<Object[]> arrayList;
		// Заголовки
		String[] header;

		MyModel(Object[][] obj, String[] header) {
			this.header = header;
			arrayList = new ArrayList<>();
			Collections.addAll(arrayList, obj);
		}

		// Метод необходимо перегружать.
		public int getRowCount() {
			return arrayList.size();
		}

		// Метод необходимо перегружать. Количество столбцов.
		public int getColumnCount() {
			return header.length;
		}

		// Метод необходимо перегружать. Получение объекта по индексу rowIndex
		public Object getValueAt(int rowIndex, int columnIndex) {
			return arrayList.get(rowIndex)[columnIndex];
		}

		// Метод необходимо перегружать. Имя колонки.
		public String getColumnName(int index) {
			return header[index];
		}

		// Устанавливаем в тело таблицы матрицу
		public void setArray(String[][] array) {
			arrayList.clear();
			Collections.addAll(arrayList, array);
			fireTableDataChanged();
		}

		// Добавляем новую строку в таблицу
		public void addRow(String[] array) {
			arrayList.add(array);
			fireTableDataChanged();
		}

		// Очищаем таблицу
		public void flush() {
			arrayList.clear();
			fireTableDataChanged();
		}
	}

}